function[al_struct,trn_err_seen_vec,trn_err_unseen_vec,tst_err_vec]=vc_upal(trn_data,tst_data,trn_labels,tst_labels,LOSS_FUNCTION,BUDGET,K)

    %if(strcmp(LOSS_FUNCTION,'logistic'))
    %LOSS=@(p) log1p(exp(-p));
    %DERIVLOSS=@(p) -1./(1+exp(p));
    %end


% This piece of code performs VC-UPAL
[al_struct,trn_err_seen_vec,trn_err_unseen_vec,tst_err_vec]=...
    ActiveLearning(trn_data,tst_data,trn_labels,...
    tst_labels,LOSS_FUNCTION,BUDGET,K);
end

function[al_struct,trn_err_seen_vec,trn_err_unseen_vec,tst_err_vec]=...
    ActiveLearning(trn_data,tst_data,trn_labels,...
    tst_labels,LOSS_FUNCTION,BUDGET,K)

% We shall use a structure to store everything relevant

[al_struct]=CreateStructure(trn_data,tst_data,trn_labels,tst_labels,...
    LOSS_FUNCTION,BUDGET);
trn_err_seen_vec=ones(BUDGET,1);
trn_err_unseen_vec=ones(BUDGET,1);
tst_err_vec=ones(BUDGET,1);
for iter=1:BUDGET
    p_min=CalculateMinProbability(al_struct,iter);
    prob_querying=AssignProbability(al_struct,p_min);
    norm_entropy=CalculateNormalizedEntropy(prob_querying);
    selected_index=randp(prob_querying,1);
    if(selected_index==0)
        display('selected index is zero!!!');
    end
    % Now that we have queried a point, update the structure
    al_struct=UpdateStructure(al_struct,prob_querying,...
                              selected_index,iter,K,norm_entropy);
    w_vec=Optimize(al_struct);
    % Update this new vector in the structure too
    al_struct.w_vec=w_vec;
    [trn_err_seen,trn_err_unseen,tst_err]=...
        CalculateTrainAndTestError(al_struct);

    trn_err_seen_vec(iter)=trn_err_seen;
    trn_err_unseen_vec(iter)=trn_err_unseen;
    tst_err_vec(iter)=tst_err;
end
end

function[p_min]=CalculateMinProbability(al_struct,iter)
num_trn=al_struct.num_trn;
budget=al_struct.BUDGET;
p_min=1/(num_trn*budget);
end

function[prob_querying]=AssignProbability(al_struct,p_min)

trn_data=al_struct.trn_data;
w_vec=al_struct.w_vec;
unqueried_indices=al_struct.unqueried_indices;
num_trn=al_struct.num_trn;
num_unqueried=length(al_struct.unqueried_indices);
model_al_unqueried_points=trn_data(:,al_struct.unqueried_indices)'*w_vec;

%p_vec=CONDPROB(model_al_unqueried_points);
%loss_vec_unqueried_points=...
 %   sqrt(p_vec*CalculateLoss(model_al_unqueried_points').^2+...
  % (1-p_vec)*CalculateLoss(-model_al_unqueried_points').^2);

loss_vec_unqueried_points=CalculateLoss(-abs(model_al_unqueried_points));
u_vector_unqueried_indices=al_struct.u_vector(unqueried_indices);
ratio_vec=loss_vec_unqueried_points./u_vector_unqueried_indices;
prob_querying=zeros(num_trn,1);
prob_querying(unqueried_indices)=p_min+...
    (1-num_unqueried*p_min)*ratio_vec/sum(ratio_vec);

if(sum(~isfinite(prob_querying))>=1)
    display(model_al_unqueried_points);
    display(loss_vec_unqueried_points');
end
end

% This function is called in order to update the structure after each new
% query has been made.

function[al_struct]=UpdateStructure(al_struct,prob_querying,...
    selected_index,iter,K,norm_entropy)

al_struct.norm_entropy_vec(iter)=norm_entropy;

% Update queried_bool,queried_indices,and unqueried indices.
[al_struct]=UpdateVectors(al_struct,selected_index);

% Update the importance weights
p=prob_querying(selected_index);
[al_struct]=UpdateImportanceWeights(al_struct,iter,selected_index,p);

% Update the u vector
[al_struct]=UpdateUVectorProbabilities(al_struct,prob_querying);
[al_struct]=UpdateLambda(al_struct,iter,K);
end


function[al_struct]=UpdateLambda(al_struct,iter,K)
% get importance weights

if(mod(iter,15)~=0)
    return;
end
start=-4;
stop=4;
range=start:stop;
cv_err_rate=ones(1,length(range));
base=6;
best_err_rate=1;
best_param=start;
imp_weights=al_struct.importance_weights;
num_trn=al_struct.num_trn;
imp_weights=imp_weights*num_trn*iter;

for iter=1:length(range);
    lambda_val=base^range(iter)/norm(imp_weights);
    err_rate=CrossValidate(al_struct,lambda_val);
    cv_err_rate(iter)=err_rate;
    
    if(err_rate<best_err_rate)
        best_err_rate=err_rate;
        best_param=range(iter);
    end
end
%display(cv_err_rate);
%display(best_param);
al_struct.lambda_t=base^best_param/(norm(imp_weights,2));
end

function[al_struct]=UpdateUVectorProbabilities(al_struct,prob_querying)

% Here we update the u_vector which is (1-p^{t})...(1-p^1)
% The u vector is useful only for the unqueried points. For 
% points, that have been queried in the past we shall not be using u_vector.
% For the point that has been queried now, we shall use u_vector to update
% importance weight.

% If the point has not been queried yet, then this quantity is simply 
% (1-p^t)......(1-p^1). Note that the piece of code below will update even
% the u_vector component corresponding to the index that was queried. This
% should be fine since we will not be using it

u_vector=al_struct.u_vector;
al_struct.u_vector=u_vector.*(1-prob_querying);
end

function[al_struct]=UpdateImportanceWeights(al_struct,iter,selected_index,p)

num_trn=al_struct.num_trn;
queried_indices=al_struct.queried_indices;
al_struct.importance_weights(queried_indices)=...
    al_struct.importance_weights(queried_indices)*(iter-1)/iter;

% Notice that the importance weight for the just queried point 
%(selected index), needs to be calculated from scratch using u vector.

al_struct.importance_weights(selected_index)=...
    1/(num_trn*iter*al_struct.u_vector(selected_index)*p);
end


function[al_struct]=UpdateVectors(al_struct,selected_index)
al_struct.queried_bool_vec(selected_index)=1;
al_struct.queried_indices=find(al_struct.queried_bool_vec);
al_struct.unqueried_indices=find(~al_struct.queried_bool_vec);
end


function[w_vec]=Optimize(al_struct,xtrn,ytrn,imp_weights,lambda_t)

%The objective is emploss(w)+lambda_t R(w)
% emprisk(w) is the importance weighted loss of w, and 

radius=al_struct.radius;

if(nargin==1)
    queried_indices=al_struct.queried_indices;
    imp_weights=al_struct.importance_weights(queried_indices);
    xtrn=al_struct.trn_data(:,queried_indices);
    ytrn=al_struct.trn_labels(queried_indices);
    lambda_t=al_struct.lambda_t;
end

YX=xtrn*diag(ytrn);
YW=ytrn.*imp_weights;
%size(YX)
%size(al_struct.w_vec)
%size(imp_weights_qrd')
%display(CalculateLoss(al_struct.w_vec'*YX));
EMPRISK= @(w) sum(imp_weights'.*CalculateLoss(w'*YX));
REG=@(w) -log(radius^2-norm(w)^2);

GRADEMPRISK=...
    @(w) xtrn*(YW.*CalculateDerivLoss(w'*YX)');

% Remember our regularizer is -log(R^2-||w||^2).
GRADREG=@(w) 2*w/(radius^2-norm(w)^2);


NONLCONineq=@(w) norm(w,2)^2-radius^2;
GRADNONLCONineq=@(w) 2*w; 

NONLCON=@(w) deal(NONLCONineq(w),[],GRADNONLCONineq(w),[]); 

%The objective is emploss(w)+lambda_t R(w)
% emprisk(w) is the importance weighted loss of w, and R(w) is the
% self-concordant barrier


OBJ=@(w) EMPRISK(w)+lambda_t*REG(w); 
GRAD=@(w) GRADEMPRISK(w)+lambda_t*GRADREG(w);
OBJGRAD=@(w) deal(OBJ(w),GRAD(w));

% optimize. 
%optionscon=optimset('Display','notify','GradObj','on',...
 %   'TolFun',10^-6,'Algorithm','interior-point','GradConstr','on',...
 %  'MaxFunEvals',3000,'LargeScale','off');

% Start from the old vector. 
w_0=al_struct.w_vec;

%%%% CONSTRAINED MINIMIZATION

%A=[];b=[];Aeq=[];beq=[];lb=[];ub=[];

%[w_vec,~,exitflag,~,~]=fmincon(OBJGRAD,w_0,A,b,Aeq,beq,lb,ub,NONLCON,optionscon);


% UNCONSTRAINED MINIMIZATION......
%optionsuncon=optimset('Display','off','GradObj','on',...
 %   'TolFun',10^-7,'LargeScale','off');
%[w_vec,~,exitflag,output,grad]=fminunc(OBJGRAD,w_0,optionsuncon);


% ERIC SCHMIDT's optimizier

options.Display='off';
options.MaxFunEvals=1000;
options.MaxIter=500;
[temp1,temp2]=system('hostname');
if(~strcmp(strtrim(temp2),'leibniz'))
    % Then do not use mex for now
       options.useMex=0;
end

[w_vec,f,exitflag,output]=minFunc(OBJGRAD,w_0,options);

if(exitflag==0)
    %display(exitflag);
    al_struct.num_defaults=al_struct.num_defaults+1;
    %display(output.message)
end

norm_w_vec=norm(w_vec);
if(norm_w_vec>radius)
    display(strcat('Radius is ',num2str(radius)));
    display(strcat('Norm of w_vec is ',num2str(norm_w_vec)));
    %display(w_vec');
    %display('POTENTIAL ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!.....');
    %display('regularization at w_vec is ')
    %display(REG(w_vec));
    display('Objective value at w_vec is ')
    display(OBJ(w_vec));
    
    %display('Gradient of regularizer at w-vec is ...');
    %display(GRAD(w_vec)');
    %display('Norm of gradient vector is ');
    %display(norm(GRAD(w_vec)));
    
    display(output);
    %display(output.message);
end
end

function[al_struct]=CreateStructure(trn_data,tst_data,trn_labels,...
    tst_labels,LOSS_FUNCTION,BUDGET)

al_struct=struct();
al_struct.trn_data=trn_data;
al_struct.tst_data=tst_data;
al_struct.trn_labels=trn_labels;
al_struct.tst_labels=tst_labels;


num_trn=size(trn_data,2);
num_tst=size(tst_data,2);
num_dims=size(trn_data,1);
al_struct.num_trn=num_trn;
al_struct.num_tst=num_tst;
al_struct.num_dims=num_dims;

al_struct.BUDGET=BUDGET;
al_struct.radius=num_trn^0.5;
al_struct.lambda_t=1;
al_struct.LOSS_FUNCTION=LOSS_FUNCTION;

% These are the quantities that need to be updated after each and 
% every round.
al_struct.w_vec=zeros(num_dims,1);
al_struct.queried_bool_vec=zeros(num_trn,1);
al_struct.queried_indices=[];
al_struct.unqueried_indices=1:num_trn;
al_struct.u_vector=ones(num_trn,1);
al_struct.importance_weights=zeros(num_trn,1);
al_struct.norm_entropy_vec=zeros(BUDGET,1);


% Just a counter of how many times we utilized all out iterations for
% gradient descent
al_struct.num_defaults=0;
end

function[trn_err_seen,trn_err_unseen,tst_err]=CalculateTrainAndTestError(al_struct)

w_vec=al_struct.w_vec;
tst_data=al_struct.tst_data;
tst_labels=al_struct.tst_labels;

trn_data=al_struct.trn_data;
trn_labels=al_struct.trn_labels;
num_tst=al_struct.num_tst;

pred_tst=sign(w_vec'*tst_data);
tst_err=sum(sign(tst_labels')~=sign(pred_tst))/num_tst;

pred_trn=sign(w_vec'*trn_data);

queried_indices=al_struct.queried_indices;
unqueried_indices=al_struct.unqueried_indices;
num_queried_points=length(queried_indices);
num_unqueried_points=length(unqueried_indices);

trn_err_seen=sum(sign(pred_trn(queried_indices)')~=...
                 sign(trn_labels(queried_indices)))/num_queried_points;

trn_err_unseen=sum(sign(pred_trn(unqueried_indices))~=...
                   sign(trn_labels(unqueried_indices)'))/...
    num_unqueried_points;

end


function[avg_tst_err]=CrossValidate(al_struct,lambda_t)

MAX_NUM_FOLDS=5;
avg_tst_err=0;

size_fold=floor(length(al_struct.queried_indices)/MAX_NUM_FOLDS);
for fold_num=1:MAX_NUM_FOLDS
    ftst_start=size_fold*(fold_num-1)+1;
    ftst_stop=ftst_start+size_fold-1;
    queried_indices=al_struct.queried_indices;
    ftrn=[1:ftst_start-1,ftst_stop+1:length(queried_indices)];
    ftst=ftst_start:ftst_stop;
    ftrn_indices=al_struct.queried_indices(ftrn);
    ftst_indices=al_struct.queried_indices(ftst);
    
    trn_data_fold=al_struct.trn_data(:,ftrn_indices);
    tst_data_fold=al_struct.trn_data(:,ftst_indices);
    
    trn_labels_fold=al_struct.trn_labels(ftrn_indices);
    tst_labels_fold=al_struct.trn_labels(ftst_indices);
    
    imp_weights=al_struct.importance_weights(ftrn_indices);
    % We will run our optimizer
    w_vec=Optimize(al_struct,trn_data_fold,trn_labels_fold,imp_weights,lambda_t);
    
    % Calculate test error for this fold
    num_tst=length(tst_labels_fold);
    if num_tst~=size_fold
        display('MISTAKE....');
        display(num_tst);
        display(size_fold);
        display(ftst);
        display(tst_labels_fold);
    end
    pred_tst=sign(w_vec'*tst_data_fold);
    tst_err=sum(sign(tst_labels_fold')~=sign(pred_tst))/num_tst;
    avg_tst_err=avg_tst_err+tst_err;
end
avg_tst_err=avg_tst_err/MAX_NUM_FOLDS;
end




function[norm_entropy]=CalculateNormalizedEntropy(prob_querying)
indices=prob_querying>0;
num_indices=sum(indices);
norm_entropy=-sum(prob_querying(indices).*log(prob_querying(indices)))/log(num_indices);
end

function[loss_vec]=CalculateLoss(p_vec)
num_rows=size(p_vec,1);
num_cols=size(p_vec,2);
loss_vec=zeros(num_rows,num_cols);
indices=p_vec<-10;
loss_vec(indices)=-p_vec(indices);
loss_vec(~indices)=log1p(exp(-p_vec(~indices)));
end

function[deriv_vec]=CalculateDerivLoss(p_vec)
num_rows=size(p_vec,1);
num_cols=size(p_vec,2);
deriv_vec=zeros(num_rows,num_cols);
indices=p_vec<-10;
deriv_vec(indices)=-1;
deriv_vec(~indices)=-1./(1+exp(p_vec(~indices)));
end

function[cond_prob]=CONDPROB(f)
cond_prob=1./(1+exp(-f));
end
