[temp1, temp2]=system('hostname');
if(strcmp(strtrim(temp2),'leibniz'))
    path='/home/gmravi/';
else
    path='/net/hu17/gmravi/';
end

dirpath=strcat(path,'matlab_codes/iwal/pima/');
trn_data_file=strcat(dirpath,'pima_train_data.txt');
tst_data_file=strcat(dirpath,'pima_test_data.txt');
trn_labels_file=strcat(dirpath,'pima_train_labels.txt');
tst_labels_file=strcat(dirpath,'pima_test_labels.txt');

[trn_data,tst_data,trn_labels,tst_labels]=...
    ReadData(trn_data_file,tst_data_file,trn_labels_file,tst_labels_file);

num_trn=size(trn_data,2);
num_tst=size(tst_data,2);
trn_data=[trn_data;ones(1,num_trn)];
tst_data=[tst_data;ones(1,num_tst)];

BUDGET=300;
range=10:10;
NUM_REPEATS=1;
stream = RandStream('mt19937ar','Seed',sum(100*clock));
RandStream.setDefaultStream(stream);
avg_tst_err_k=zeros(length(range),1);
display(trn_data_file);
display(range);
num_defaults_avg_k=zeros(length(range),1);
norm_entropy_vec=zeros(BUDGET,1);

for iter=1:length(range)
    k=1/2^range(iter);
    avg_of_tst_err_vec=zeros(BUDGET,1);
    num_defaults_avg=0;
    for repeat_num=1:NUM_REPEATS
        stream = RandStream('mt19937ar','Seed',sum(100*clock));
        RandStream.setDefaultStream(stream);
        LOSS_FUNCTION='logistic';
        [al_struct,trn_err_seen_vec,trn_err_unseen_vec,tst_err_vec]=...
            vc_upal_rep(trn_data,tst_data,trn_labels,...
                    tst_labels,LOSS_FUNCTION,BUDGET,k);
        avg_of_tst_err_vec=avg_of_tst_err_vec+tst_err_vec;
        num_defaults_avg=num_defaults_avg+al_struct.num_defaults;
        norm_entropy_vec=norm_entropy_vec+al_struct.norm_entropy_vec;
    end
    norm_entropy_vec=norm_entropy_vec/NUM_REPEATS;
    num_defaults_avg_k(iter)=num_defaults_avg/NUM_REPEATS;
    avg_of_tst_err_vec=avg_of_tst_err_vec/NUM_REPEATS;
    avg_tst_err_k(iter)=avg_of_tst_err_vec(end);
end

display(num_defaults_avg_k');
display(avg_tst_err_k');
%display(avg_of_tst_err_vec(end-5:end)');
%display(al_struct.lambda_t);
%h=plot(avg_of_tst_err_vec);
%figname=strcat('L2-',num2str(k),'.fig');
%title(figname);
%saveas(h,figname,'fig');
