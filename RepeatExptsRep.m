[temp1, temp2]=system('hostname');
if(strcmp(strtrim(temp2),'leibniz'))
    path='/home/gmravi/';
else
    path='/net/hu17/gmravi/';
    N_TO_USE=8;
    LASTN=maxNumCompThreads(N_TO_USE); 
end

display('RUNNING UPAL WITH REPETITIONS.....');
dirpath=strcat(path,'matlab_codes/iwal/abalone/');
trn_data_file=strcat(dirpath,'abalone_train_data.txt');
tst_data_file=strcat(dirpath,'abalone_test_data.txt');
trn_labels_file=strcat(dirpath,'abalone_train_labels.txt');
tst_labels_file=strcat(dirpath,'abalone_test_labels.txt');

[trn_data,tst_data,trn_labels,tst_labels]=...
    ReadData(trn_data_file,tst_data_file,trn_labels_file,tst_labels_file);

num_trn=size(trn_data,2);
num_tst=size(tst_data,2);
trn_data=[trn_data;ones(1,num_trn)];
tst_data=[tst_data;ones(1,num_tst)];

BUDGET=300;
range=2;
lambda_sq_range=[10^-4,10^-3,10^-2,10^-1,1];
NUM_REPEATS=10;
LOSS_FUNCTION='logistic';
avg_tst_err_k=zeros(length(range),1);
display(trn_data_file);
display(range);
num_defaults_avg_k=zeros(length(range),1);
avg_of_tst_err_mat=zeros(BUDGET,length(range));

for iter=1:length(lambda_sq_range)
    avg_of_tst_err_vec=zeros(BUDGET,1);
    avg_of_trn_err_unseen_vec=zeros(BUDGET,1);
    avg_of_trn_err_seen_vec=zeros(BUDGET,1);
    
    num_defaults_avg=0;
    stream = RandStream('mt19937ar','Seed',sum(100*clock));
    RandStream.setDefaultStream(stream);
    norm_entropy_vec=zeros(BUDGET,1);
    for repeat_num=1:NUM_REPEATS
        stream = RandStream('mt19937ar','Seed',sum(100*clock));
        
        [al_struct]=...
            vc_upal_rep(trn_data,tst_data,trn_labels,...
                    tst_labels,LOSS_FUNCTION,BUDGET,range,lambda_sq_range(iter));
        avg_of_tst_err_vec=avg_of_tst_err_vec+al_struct.tst_err_vec;
        avg_of_trn_err_unseen_vec=...
            avg_of_trn_err_unseen_vec+al_struct.trn_err_unseen_vec;
        
        avg_of_trn_err_seen_vec=...
            avg_of_trn_err_seen_vec+al_struct.trn_err_seen_vec;
        
        num_defaults_avg=num_defaults_avg+al_struct.num_defaults;
        norm_entropy_vec=norm_entropy_vec+al_struct.norm_entropy_vec;
    end
    display(strcat('Finished iter=',num2str(iter)));
    norm_entropy_vec=norm_entropy_vec/NUM_REPEATS;
    num_defaults_avg_k(iter)=num_defaults_avg/NUM_REPEATS;
    avg_of_tst_err_vec=avg_of_tst_err_vec/NUM_REPEATS;
    avg_of_tst_err_mat(:,iter)=avg_of_tst_err_vec;
    avg_of_trn_err_unseen_vec=avg_of_trn_err_unseen_vec/NUM_REPEATS;
    avg_of_trn_err_seen_vec=avg_of_trn_err_seen_vec/NUM_REPEATS;
    avg_tst_err_k(iter)=avg_of_tst_err_vec(end);
end

display(avg_tst_err_k');
save('~/matlab_codes/VC_UPAL/expt_results/abalone/vc_upal_rep_abalone_hinge.mat');
display(al_struct.w_vec');
display(al_struct.obj_value(end));
%plotme;
%display(avg_of_tst_err_vec(end-5:end)');
%display(al_struct.lambda_t);
%h=plot(avg_of_tst_err_vec);
%figname=strcat('L2-',num2str(k),'.fig');
%title(figname);
%saveas(h,figname,'fig');
