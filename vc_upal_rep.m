function[al_struct]=...
        vc_upal_rep(trn_data,tst_data,trn_labels,...
                    tst_labels,LOSS_FUNCTION,BUDGET,K,lambda_sq)
% This piece of code performs VC-UPAL but  WITH REPETITIONS...
[al_struct]=...
    ActiveLearning(trn_data,tst_data,trn_labels,...
    tst_labels,LOSS_FUNCTION,BUDGET,K,lambda_sq);
end

function[al_struct]=ActiveLearning(trn_data,tst_data,trn_labels,...
                                   tst_labels,LOSS_FUNCTION,BUDGET,K,lambda_sq)

% We shall use a structure to store everything relevant
[al_struct]=CreateStructure(trn_data,tst_data,trn_labels,tst_labels,...
    LOSS_FUNCTION,BUDGET);
while(length(al_struct.queried_indices)<BUDGET)
    p_min=CalculateMinProbability(al_struct);
    prob_querying=AssignProbability(al_struct,p_min);
    norm_entropy=CalculateNormalizedEntropy(prob_querying);
    selected_index=randp(prob_querying,1);
    if(selected_index==0)
        display('selected index is zero!!!');
    end
    
    % Now that we have queried a point, update the structure
    al_struct=UpdateStructure(al_struct,prob_querying,...
                              selected_index,K,norm_entropy);
    [al_struct]=Optimize(al_struct,lambda_sq);
    
    [trn_err_seen,trn_err_unseen,tst_err]=...
        CalculateTrainAndTestError(al_struct);
    
    [trn_loss_seen,trn_loss_unseen,tst_loss]=...
        CalculateTrainAndTestLoss(al_struct);
    
    %display(tst_err);
    al_struct=UpdateErrorsAndLosses(al_struct,trn_err_seen,trn_err_unseen,...
                                    tst_err,trn_loss_seen,trn_loss_unseen,...
                                    tst_loss);
end
end

function[al_struct]=UpdateLambda(al_struct,K)

% get importance weights
nt=al_struct.num_trn*al_struct.iter;
reg_pow=1/3;
al_struct.lambda_t=(10^K)*nt/(sum(al_struct.imp_weights))^reg_pow;
return;

start=0;
stop=2;
range=start:stop;
cv_err_rate=ones(1,length(range));
base=10;
best_err_rate=1;
best_param=start;
imp_weights=al_struct.imp_weights;
num_trn=al_struct.num_trn;
imp_weights=imp_weights*num_trn*iter;

for iter=1:length(range);
    lambda_val=base^range(iter)/norm(imp_weights);
    err_rate=CrossValidate(al_struct,lambda_val);
    cv_err_rate(iter)=err_rate;
    
    if(err_rate<best_err_rate)
        best_err_rate=err_rate;
        best_param=range(iter);
    end
end
%display(cv_err_rate);
%display(best_param);
al_struct.lambda_t=base^best_param/(norm(imp_weights,2));
end



function[al_struct]=Optimize(al_struct,lambda_sq)

%The objective is emploss(w)+lambda_t R(w)
% emprisk(w) is the importance weighted loss of w, and 

queried_indices=al_struct.queried_indices;
imp_weights=al_struct.imp_weights(queried_indices);
sum_square_imp_weights=al_struct.sum_square_imp_weights(queried_indices);
xtrn=al_struct.trn_data(:,queried_indices);
ytrn=al_struct.trn_labels(queried_indices);
lambda_t=al_struct.lambda_t;

radius=al_struct.radius;
YX=xtrn*diag(ytrn);
YW=ytrn.*imp_weights;

% Subtracting the variance term from the risk 
C_v=sqrt(log(al_struct.iter))/10;

al_struct.C_v=C_v;
VAR=@(w) max(sum(sum_square_imp_weights.*CalculateLoss(w'*YX,al_struct)'.^2)-sum(CalculateLoss(w'*YX,al_struct))^2,0);
EMPRISK=@(w) sum(imp_weights'.*CalculateLoss(w'*YX,al_struct));

LBRISK= @(w) EMPRISK(w)-C_v*sqrt(VAR(w));
REG=@(w) -log(radius^2-norm(w(1:end-1))^2);

% This is the gradient of empirical risk
GRADEMPRISK=...
    @(w) xtrn*(YW.*CalculateDerivLoss(w'*YX,al_struct)');

% The gradient of the square root of variance. I am assuming that VAR(w)
% always returns a strictly positive quantity. Most of the times this
% assumption should hold true

temp1=YX*diag(sum_square_imp_weights);
DERIVVAR=@(w) temp1*(CalculateLoss(w'*YX,al_struct)'.*CalculateDerivLoss(w'*YX,al_struct)')-...
    YX*(CalculateDerivLoss(w'*YX,al_struct)')*(sum(CalculateLoss(w'*YX,al_struct)));
GRADSQRTVAR=@(w) DERIVVAR(w)/(sqrt(VAR(w)));

% Remember our regularizer is -log(R^2-||w||^2).
GRADREG=@(w) 2*[w(1:end-1);0]/(radius^2-norm(w(1:end-1))^2) ;

%The objective is emploss(w)+lambda_t R(w)
% emprisk(w) is the importance weighted loss of w, and R(w) is the
% self-concordant barrier

OBJ=@(w) LBRISK(w)+lambda_t*REG(w)+lambda_sq*norm(w(1:end-1))^2; 

GRADOBJ=@(w) GRADEMPRISK(w)+lambda_t*GRADREG(w)-C_v*Clip(GRADSQRTVAR(w))+ 2*lambda_sq*[w(1:end-1);0];
OBJGRAD=@(w) deal(OBJ(w),GRADOBJ(w));
BALLPROJ=@(w) ProjectOnBall(w,al_struct);
radius=al_struct.radius;

% optimize. 
w_0=al_struct.w_vec;
%w_0=zeros(al_struct.num_dims,1);
%w_0=randn(al_struct.num_dims,1);
optimizer='fmincon';
if(strcmp(optimizer,'fmincon'))
    optionscon=optimset('Display','notify','GradObj','on',...
    'TolFun',10^-4,'Algorithm','interior-point','GradConstr','on',...
    'MaxFunEvals',1000,'MaxIter',500);
    NONLCONineq=@(w) norm(w(1:end-1),2)^2-radius^2;
    GRADNONLCONineq=@(w) 2*[w(1:end-1);0]; 
    NONLCON=@(w) deal(NONLCONineq(w),[],GRADNONLCONineq(w),[]); 
    
    A=[];b=[];Aeq=[];beq=[];lb=[];ub=[];
    [w_vec,~,exitflag,output]=...
        fmincon(OBJGRAD,w_0,A,b,Aeq,beq,lb,ub,NONLCON,optionscon);
end
if(strcmp(optimizer,'minfunc'))
    % ERIC SCHMIDT's optimizier
    options.Display='off';
    options.MaxFunEvals=1000;
    options.MaxIter=500;
    [~,temp2]=system('hostname');
    if(~strcmp(strtrim(temp2),'leibniz'))
        % Then do not use mex for now
           options.useMex=0;
    end
    [w_vec,fval]=minFunc(OBJGRAD,w_0,options);
    al_struct.obj_value(sum(al_struct.queried_bool_vec))=fval;
    %display(w_vec');
end

if(strcmp(optimizer,'minConf'))
    % ERic schmidt's constrained minimizer
    options.verbose=0;
    options.maxIter=500;
    options.interp=0;
    [~,temp2]=system('hostname');
    if(~strcmp(strtrim(temp2),'leibniz'))
        % Then do not use mex for now
           options.useMex=0;
    end
    [w_vec]=minConf_PQN(OBJGRAD,w_0,BALLPROJ,options);
end

% Finally set the w_vec in al_struct
al_struct.w_vec=w_vec;


%if(exitflag==0)
 %   %display(exitflag);
  %  al_struct.num_defaults=al_struct.num_defaults+1;
    %display(output.message)
%end

norm_w_vec=norm(w_vec);

if(norm_w_vec>radius*0.999)   
    %display(output);
    %display(output.message);
    %display(norm_w_vec);
    %display(radius);
    al_struct.num_defaults=al_struct.num_defaults+1;
    % Call constrained minimizer
end
end

function[p_min]=CalculateMinProbability(al_struct)
num_trn=al_struct.num_trn;
budget=al_struct.BUDGET;
p_min=1/(num_trn*budget);
end

function[prob_querying]=AssignProbability(al_struct,p_min)
assign_scheme='not_greedy';
if(strcmp(assign_scheme,'almost_greedy'))
    w_vec=al_struct.w_vec;
    prob_querying=(epsilon/(al_struct.num_trn-1))*ones(al_struct.num_trn,1);
    % TO the nearest point assign the remaining probability.
    model_al_unqueried_points=...
        al_struct.trn_data(:,al_struct.unqueried_indices)'*w_vec;
    [~,max_index]=max(abs(model_al_unqueried_points));
    prob_querying(al_struct.unqueried_indices(max_index))=1-epsilon;
    return;
end

if(size(al_struct.queried_indices)==0)
    prob_querying=ones(al_struct.num_trn,1)/al_struct.num_trn;
    return;
end

trn_data=al_struct.trn_data;
w_vec=al_struct.w_vec;
unqueried_indices=al_struct.unqueried_indices;
queried_indices=al_struct.queried_indices;
num_trn=al_struct.num_trn;
model_al_unqueried_points=trn_data(:,al_struct.unqueried_indices)'*w_vec;

% For points which have been queried in the past we already have some
% labels


model_al_queried_points=...
    al_struct.trn_labels(queried_indices).*(trn_data(:,al_struct.queried_indices)'*w_vec);

loss_vec=zeros(num_trn,1);
loss_vec(unqueried_indices)=CalculateLoss(abs(model_al_unqueried_points),al_struct);
loss_vec(queried_indices)=CalculateLoss(model_al_queried_points,al_struct);
if(sum(loss_vec)==0)
  prob_querying=ones(num_trn,1)/num_trn;
  else
    prob_querying=p_min+(1-num_trn*p_min)*loss_vec/sum(loss_vec);
end

if(sum(~isfinite(prob_querying))>=1)
  display(prob_querying');
end
end

% This function is called in order to update the structure after each new
% query has been made.

function[al_struct]=UpdateStructure(al_struct,prob_querying,...
    selected_index,K,norm_entropy)

% First update the iteration number
al_struct.iter=al_struct.iter+1;

% Update queried_bool,queried_indices,and unqueried indices.
[al_struct]=UpdateVectors(al_struct,selected_index);

% Update the importance weights
p=prob_querying(selected_index);

[al_struct]=UpdateImportanceWeights(al_struct,selected_index,p);
[al_struct]=UpdateLambda(al_struct,K);
al_struct.norm_entropy_vec(length(al_struct.queried_indices))=norm_entropy;
end


function[al_struct]=UpdateImportanceWeights(al_struct,selected_index,p)

old_imp_weight=al_struct.imp_weights(selected_index);
old_sum_square_imp_weights=...
    al_struct.sum_square_imp_weights(selected_index);
al_struct.imp_weights(selected_index)=old_imp_weight+1/p;
al_struct.sum_squares_imp_weights=...
    old_sum_square_imp_weights+1/p^2;
end


function[al_struct]=UpdateVectors(al_struct,selected_index)
al_struct.queried_bool_vec(selected_index)=1;
al_struct.queried_indices=find(al_struct.queried_bool_vec);
al_struct.unqueried_indices=find(~al_struct.queried_bool_vec);
end

function[al_struct]=CreateStructure(trn_data,tst_data,trn_labels,...
    tst_labels,LOSS_FUNCTION,BUDGET)

al_struct=struct();
al_struct.trn_data=trn_data;
al_struct.tst_data=tst_data;
al_struct.trn_labels=trn_labels;
al_struct.tst_labels=tst_labels;


num_trn=size(trn_data,2);
num_tst=size(tst_data,2);
num_dims=size(trn_data,1);
al_struct.num_trn=num_trn;
al_struct.num_tst=num_tst;
al_struct.num_dims=num_dims;

al_struct.BUDGET=BUDGET;
al_struct.radius=num_trn^0.5;
al_struct.lambda_t=1;
al_struct.LOSS_FUNCTION=LOSS_FUNCTION;
al_struct.C_v=1;

% These are the quantities that need to be updated after each and 
% every round.
al_struct.w_vec=zeros(num_dims,1);
al_struct.queried_bool_vec=zeros(num_trn,1);
al_struct.queried_indices=[];
al_struct.unqueried_indices=1:num_trn;
al_struct.imp_weights=zeros(num_trn,1);
al_struct.sum_square_imp_weights=zeros(num_trn,1);
al_struct.norm_entropy_vec=zeros(BUDGET,1);

% Just a counter to tell us how many iterations have been made
al_struct.iter=0;

% Just a counter of how many times the optimizer did not seem to work
% properly
al_struct.num_defaults=0;

% The train and test losses and error rates
al_struct.trn_err_seen_vec=zeros(BUDGET,1);
al_struct.trn_err_unseen_vec=zeros(BUDGET,1);
al_struct.tst_err_vec=zeros(BUDGET,1);

al_struct.trn_loss_seen_vec=zeros(BUDGET,1);
al_struct.trn_loss_unseen_vec=zeros(BUDGET,1);
al_struct.tst_loss_vec=zeros(BUDGET,1);

al_struct.obj_value=zeros(BUDGET,1);

end

function[trn_err_seen,trn_err_unseen,tst_err]=...
    CalculateTrainAndTestError(al_struct)

w_vec=al_struct.w_vec;
tst_data=al_struct.tst_data;
tst_labels=al_struct.tst_labels;

trn_data=al_struct.trn_data;
trn_labels=al_struct.trn_labels;
num_tst=al_struct.num_tst;

pred_tst=sign(w_vec'*tst_data);
tst_err=sum(sign(tst_labels')~=sign(pred_tst))/num_tst;

pred_trn=sign(w_vec'*trn_data);

queried_indices=al_struct.queried_indices;
unqueried_indices=al_struct.unqueried_indices;
num_queried_points=length(queried_indices);
num_unqueried_points=length(unqueried_indices);

trn_err_seen=sum(sign(pred_trn(queried_indices)')~=...
                 sign(trn_labels(queried_indices)))/num_queried_points;

trn_err_unseen=sum(sign(pred_trn(unqueried_indices))~=...
                   sign(trn_labels(unqueried_indices)'))/...
    num_unqueried_points;

end

function [trn_loss_seen,trn_loss_unseen,tst_loss]=...
        CalculateTrainAndTestLoss(al_struct)
    w_vec=al_struct.w_vec;
    tst_data=al_struct.tst_data;
    tst_labels=al_struct.tst_labels;

    trn_data=al_struct.trn_data;
    trn_labels=al_struct.trn_labels;
    num_tst=al_struct.num_tst;

    model_tst=tst_labels'.*(w_vec'*tst_data);
    model_trn=trn_labels'.*(w_vec'*trn_data);
    
    tst_loss=sum(CalculateLoss(model_tst,al_struct))/(num_tst);
    trn_loss=CalculateLoss(model_trn,al_struct);

    queried_indices=al_struct.queried_indices;
    unqueried_indices=al_struct.unqueried_indices;
    num_queried_points=length(queried_indices);
    num_unqueried_points=length(unqueried_indices);
    
    trn_loss_seen=sum(trn_loss(queried_indices))/ ...
        num_queried_points;
    
    trn_loss_unseen=sum(trn_loss(unqueried_indices))/ ...
        num_unqueried_points;
    
end

function[al_struct]=UpdateErrorsAndLosses(al_struct,trn_err_seen,...
    trn_err_unseen,tst_err,trn_loss_seen,trn_loss_unseen,tst_loss)
    num_queried_points=length(al_struct.queried_indices);
    al_struct.trn_err_seen_vec(num_queried_points)=trn_err_seen;
    al_struct.trn_err_unseen_vec(num_queried_points)=trn_err_unseen;
    al_struct.tst_err_vec(num_queried_points)=tst_err;

    al_struct.trn_loss_seen_vec(num_queried_points)=trn_loss_seen;
    al_struct.trn_loss_unseen_vec(num_queried_points)=trn_loss_unseen;
    al_struct.tst_loss_vec(num_queried_points)=tst_loss;
end



function[norm_entropy]=CalculateNormalizedEntropy(prob_querying)
indices=prob_querying>0;
num_indices=sum(indices);
norm_entropy=-sum(prob_querying(indices).*log(prob_querying(indices)))/log(num_indices);
end

function[loss_vec]=CalculateLoss(p_vec,al_struct)
num_rows=size(p_vec,1);
num_cols=size(p_vec,2);
loss_vec=zeros(num_rows,num_cols);
if(strcmp(al_struct.LOSS_FUNCTION,'logistic'))
  indices=p_vec<-10;
  loss_vec(indices)=-p_vec(indices);
  loss_vec(~indices)=log1p(exp(-p_vec(~indices)));
end
if(strcmp(al_struct.LOSS_FUNCTION,'hinge'))
  loss_vec=max(1-p_vec,0);
end
if(strcmp(al_struct.LOSS_FUNCTION,'squared'))
    loss_vec=(1-p_vec).^2;
end
end

function[deriv_vec]=CalculateDerivLoss(p_vec,al_struct)
num_rows=size(p_vec,1);
num_cols=size(p_vec,2);
deriv_vec=zeros(num_rows,num_cols);
if(strcmp(al_struct.LOSS_FUNCTION,'logistic'))
  indices=p_vec<-10;
  deriv_vec(indices)=-1;
  deriv_vec(~indices)=-1./(1+exp(p_vec(~indices)));
end
if(strcmp(al_struct.LOSS_FUNCTION,'hinge'))
  indices=p_vec<1;
  deriv_vec(indices)=-1;
  deriv_vec(~indices)=0;
end
if(strcmp(al_struct.LOSS_FUNCTION,'squared'))
    deriv_vec=-2*(1-p_vec);
end
end
function[ret]=Clip(vec)
if(sum(isinf(vec))>1)
    ret=zeros(length(vec),1);
else
    ret=vec;
end
end

function[w]=ProjectOnBall(w,al_struct)
radius=al_struct.radius;
if(norm(w)>radius)
    w=w*radius/norm(w);
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GRAVEYARD%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%{
function[avg_tst_err]=CrossValidate(al_struct,lambda_t)

MAX_NUM_FOLDS=3;
avg_tst_err=0;

size_fold=floor(length(al_struct.queried_indices)/MAX_NUM_FOLDS);
for fold_num=1:MAX_NUM_FOLDS
    ftst_start=size_fold*(fold_num-1)+1;
    ftst_stop=ftst_start+size_fold-1;
    queried_indices=al_struct.queried_indices;
    ftrn=[1:ftst_start-1,ftst_stop+1:length(queried_indices)];
    ftst=ftst_start:ftst_stop;
    ftrn_indices=al_struct.queried_indices(ftrn);
    ftst_indices=al_struct.queried_indices(ftst);
    
    trn_data_fold=al_struct.trn_data(:,ftrn_indices);
    tst_data_fold=al_struct.trn_data(:,ftst_indices);
    
    trn_labels_fold=al_struct.trn_labels(ftrn_indices);
    tst_labels_fold=al_struct.trn_labels(ftst_indices);
    
    imp_weights=al_struct.imp_weights(ftrn_indices);
    % We will run our optimizer
    
    w_vec=Optimize(al_struct,trn_data_fold,trn_labels_fold,...
        imp_weights,lambda_t);
    
    % Calculate test error for this fold
    num_tst=length(tst_labels_fold);
    if num_tst~=size_fold
        display('MISTAKE....');
        display(num_tst);
        display(size_fold);
        display(ftst);
        display(tst_labels_fold);
    end
    pred_tst=sign(w_vec'*tst_data_fold);
    tst_err=sum(sign(tst_labels_fold')~=sign(pred_tst))/num_tst;
    avg_tst_err=avg_tst_err+tst_err;
end
avg_tst_err=avg_tst_err/MAX_NUM_FOLDS;
end
%}


